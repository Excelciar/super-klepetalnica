/**
 * Zamenjava kode smeška s sliko (iz oddaljenega strežnika
 * https://sandbox.lavbic.net/teaching/OIS/gradivo/{smesko}.png)
 * 
 * @param vhodnoBesedilo sporočilo, ki lahko vsebuje kodo smeška
 */
function dodajSmeske(vhodnoBesedilo) {
  var preslikovalnaTabela = {
    ";)": "wink.png",
    ":)": "smiley.png",
    "(y)": "like.png",
    ":*": "kiss.png",
    ":(": "sad.png"
  }
  for (var smesko in preslikovalnaTabela) {
    vhodnoBesedilo = vhodnoBesedilo.split(smesko).join(
      "<img src='https://sandbox.lavbic.net/teaching/OIS/gradivo/" +
      preslikovalnaTabela[smesko] + "' />");
  }
  return vhodnoBesedilo;
}

var nadimki = [];


/**
 * Čiščenje besedila sporočila z namenom onemogočanja XSS napadov
 * 
 * @param sporocilo začetno besedilo sporočila
 */
function divElementEnostavniTekst(sporocilo) {
  var jeSlika = sporocilo.match(new RegExp('https?:\/\/.*\.(png|gif|jpg)'));
  var jeSmesko = sporocilo.indexOf("https://sandbox.lavbic.net/teaching/OIS/gradivo/") > -1;
  
  if (jeSmesko) {
    sporocilo = 
      sporocilo.split("<").join("&lt;")
               .split(">").join("&gt;")
               .split("&lt;img").join("<img")
               .split("png' /&gt;").join("png' />");
    return divElementHtmlTekst(sporocilo);
  }
  else if (jeSlika)
   {
     return $('<div style="font-weight: bold"></div>').html(sporocilo);
   } 
  else {
    return $('<div style="font-weight: bold"></div>').text(sporocilo);  
  }
}


/**
 * Prikaz "varnih" sporočil, t.j. tistih, ki jih je generiral sistem
 * 
 * @param sporocilo začetno besedilo sporočila
 */
function divElementHtmlTekst(sporocilo) {
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}


/**
 * Obdelaj besedilo, ki ga uporabnik vnese v obrazec na spletni strani, kjer
 * je potrebno ugotoviti ali gre za ukaz ali za sporočilo na kanal
 * 
 * @param klepetApp objekt Klepet, ki nam olajša obvladovanje 
 *        funkcionalnosti uporabnika
 * @param socket socket WebSocket trenutno prijavljenega uporabnika
 */
function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  sporocilo = managePhotos(sporocilo);
  sporocilo = dodajSmeske(sporocilo);
  
  var sistemskoSporocilo;
  
  // Če uporabnik začne sporočilo z znakom '/', ga obravnavaj kot ukaz
  if (sporocilo.charAt(0) == '/') {
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
    }
  // Če gre za sporočilo na kanal, ga posreduj vsem članom kanala
  } else {
    sporocilo = filtirirajVulgarneBesede(sporocilo);
    klepetApp.posljiSporocilo(trenutniKanal, sporocilo);
    $('#sporocila').append(divElementEnostavniTekst(sporocilo));
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  }

  $('#poslji-sporocilo').val('');
}

// Branje vulgarnih besed v seznam
var vulgarneBesede = [];
$.get('/swearWords.txt', function(podatki) {
  vulgarneBesede = podatki.split('\r\n'); 
});

/**
* Iz podanega niza vse besede iz seznama vulgarnih besed zamenjaj z enako dolžino zvezdic (*).
* 
* @param vhodni niz
*/
function filtirirajVulgarneBesede(vhod) {

 for (var i in vulgarneBesede) {
   var re = new RegExp('\\b' + vulgarneBesede[i] + '\\b', 'gi');
   vhod = vhod.replace(re, "*".repeat(vulgarneBesede[i].length));
 }
 
 return vhod;
}


var socket = io.connect();
var trenutniVzdevek = "";
var trenutniKanal = "";

// Poačakj, da se celotna stran naloži, šele nato začni z izvajanjem kode
$(document).ready(function() {
  var klepetApp = new Klepet(socket);
  
  // Prikaži rezultat zahteve po spremembi vzdevka
  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      trenutniVzdevek = rezultat.vzdevek;
      $('#kanal').text(trenutniVzdevek + " @ " + trenutniKanal);
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
  });
  
  // Prikaži rezultat zahteve po spremembi kanala
  socket.on('pridruzitevOdgovor', function(rezultat) {
    trenutniKanal = rezultat.kanal;
    $('#kanal').text(trenutniVzdevek + " @ " + trenutniKanal);
    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
  });
  
  // Prikaži prejeto sporočilo
  socket.on('sporocilo', function (sporocilo) {
    var posiljatelj = sporocilo.uporabnik;
    console.log(sporocilo.uporabnik, sporocilo);
    if(nadimki[sporocilo.uporabnik] != undefined)
    {
      posiljatelj = nadimki[sporocilo.uporabnik] + "(" + sporocilo.uporabnik +")"
          var novElement = divElementEnostavniTekst(posiljatelj + sporocilo.besedilo);
          $('#sporocila').append(novElement);
    }
    else if(sporocilo.uporabnik == undefined)
      {
            var novElement = divElementEnostavniTekst(sporocilo.besedilo);
             $('#sporocila').append(novElement);
      }
    
    else
    {
          var novElement = divElementEnostavniTekst(posiljatelj + sporocilo.besedilo);
          $('#sporocila').append(novElement);
    }
  });
  
  // Prikaži seznam kanalov, ki so na voljo
  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var i in kanali) {
      if (kanali[i] != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanali[i]));
      }
    }
  
    // Klik na ime kanala v seznamu kanalov zahteva pridružitev izbranemu kanalu
    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });
  
  // Prikaži seznam trenutnih uporabnikov na kanalu
  socket.on('uporabniki', function(uporabniki) {
    $('#seznam-uporabnikov').empty();
    for (var i=0; i < uporabniki.length; i++) {
      if( nadimki[uporabniki[i]] != undefined)
      {
        //console.log("I did something!")
        $('#seznam-uporabnikov').append(divElementEnostavniTekst(nadimki[uporabniki[i]] + "(" + uporabniki[i] + ")"));
      }
      else
      {
        $('#seznam-uporabnikov').append(divElementEnostavniTekst(uporabniki[i]));
      }
    }
    
        // Klik na ime v seznamu zahteva krcanje I guess
    $('#seznam-uporabnikov div').click(function() {
      console.log($(this).text());
      var besedilo = $(this).text();
      var parametri = besedilo.split('(');
      
      if(parametri[1] != undefined)
      {
         var besedilo = parametri[1];
         var parametri2 = besedilo.split(')');
         
        klepetApp.procesirajUkaz('/zasebno "' + parametri2[0] + '" "' +  '☜');
        $('#poslji-sporocilo').focus();
        var sporocilo;

        sporocilo = '(zasebno za ' + parametri2[0] + '): '  + '&#9756;';

        $('#sporocila').append(divElementHtmlTekst(sporocilo));
      }
      else
      {
        klepetApp.procesirajUkaz('/zasebno "' + $(this).text() + '" "' +  '☜');
        $('#poslji-sporocilo').focus();
        var sporocilo;

        sporocilo = '(zasebno za ' + $(this).text() + '): '  + '&#9756;';

        $('#sporocila').append(divElementHtmlTekst(sporocilo));
      }

    });
  });

  socket.on('nastaviNadimek', function(uporabnik, nadimek) {
        //nadimki[uporabnik] = nadimek;
       // console.log(nadimki[uporabnik]);
        //console.log(uporabnik.uporabnik, uporabnik.nadimek);
        
        nadimki[uporabnik.uporabnik] = uporabnik.nadimek;
        //console.log(nadimki[uporabnik.uporabnik]);
  });
  
  socket.on('popraviNadimek', function(prejsnjiVzdevek, vzdevek) {
              console.log(prejsnjiVzdevek.prejsnjiVzdevek, prejsnjiVzdevek.vzdevek + " popraviNadimek");

      if(nadimki[prejsnjiVzdevek.prejsnjiVzdevek] != undefined)
      {
              console.log(prejsnjiVzdevek.prejsnjiVzdevek, prejsnjiVzdevek.vzdevek + " popraviNadimek again!");
        nadimki[prejsnjiVzdevek.vzdevek] = nadimki[prejsnjiVzdevek.prejsnjiVzdevek];
        nadimki[prejsnjiVzdevek.prejsnjiVzdevek] = undefined;
        
      }
  });
  // Seznam kanalov in uporabnikov posodabljaj vsako sekundo
  setInterval(function() {
    socket.emit('kanali');
    socket.emit('uporabniki', {kanal: trenutniKanal});
  }, 1000);

  $('#poslji-sporocilo').focus();
  
  // S klikom na gumb pošljemo sporočilo strežniku
  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
});

function managePhotos(x) 
                          
 {
    var photoID = x.match(new RegExp(/https?:\/\/\S+(.jpg|.png|.gif)/, 'g')); 
    if(photoID != null) 
    {
      var i = 0;
      while(i < photoID.length)
      {
        x = x + "<br />" + '<img src="' + photoID[i] +'"style="width:200px; margin-left:20px;">' ;
        i++;
      }
    }
    return x;
 }